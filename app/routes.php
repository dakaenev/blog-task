<?php

declare(strict_types=1);

use App\Application\Actions\User\ListUsersAction;
use App\Application\Actions\User\ViewUserAction;
use App\Application\Actions\User\UpdateUserAction;
use App\Application\Actions\User\DeleteUserAction;
use App\Application\Actions\Post\ListPostsAction;
use App\Application\Actions\Register\RegisterUserAction;
use App\Application\Actions\Login\UserLoginAction;
use App\Application\Actions\Login\UserLogoutAction;
use App\Application\Actions\Post\ViewPostAction;
use App\Application\Actions\Post\DeletePostAction;
use App\Application\Actions\Post\CreatePostAction;
use App\Application\Actions\Post\UpdatePostAction;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;


return function (App $app) {
  global $app;
  $app->options('/{routes:.*}', function (Request $request, Response $response) {
    return $response;
  });

  $app->get('/src/Uploads/{name}', function ($request, $response, $args) use ($app) {
    $dir = dirname(__DIR__) . "/src/Uploads/";
    $name = $args['name'];
    $img = file_get_contents($dir . $name);
    $response->write($img);
    return $response->withHeader('Content-Type', FILEINFO_MIME_TYPE);
  });
  $app->get('/', UserLoginAction::class);
  $app->get('/login', UserLoginAction::class)->setName('login');
  $app->post('/login', UserLoginAction::class);
  $app->get('/logout', UserLogoutAction::class)->setName('logout');
  $app->get('/register', RegisterUserAction::class)->setName('register');
  $app->post('/register', RegisterUserAction::class);

  $app->group('/users', function (Group $group) {
    $group->get('', ListUsersAction::class)->setName("listUsers");
    $group->get('/{id}', ViewUserAction::class);
    $group->get('/update/{id}', UpdateUserAction::class)->setName('update_user');
    $group->post('/update/{id}', UpdateUserAction::class);
    $group->get('/delete/{id}', DeleteUserAction::class)->setName('delete_user');
  });

  $app->group('/posts', function (Group $group) {
    $group->get('/new', CreatePostAction::class)->setName('create_post');
    $group->post('/new', CreatePostAction::class)->setName('create_post');
    $group->get('/update/{id}', UpdatePostAction::class)->setName('update_post');
    $group->get('/delete/{id}', DeletePostAction::class)->setName('delete_post');
    $group->post('/update/{id}', UpdatePostAction::class);
    $group->get('', ListPostsAction::class)->setName('listPosts');
    $group->get('/{id}', ViewPostAction::class);
  });
};

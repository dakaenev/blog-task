<?php

declare(strict_types=1);

use App\Domain\User\UserRepository;
use DI\ContainerBuilder;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        UserRepository::class => \DI\autowire(UserRepository::class),
    ]);
    $containerBuilder->addDefinitions([
        PostRepository::class => \DI\autowire(PostRepository::class),
    ]);
};

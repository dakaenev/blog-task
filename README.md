To run this project in local environment:
  0. This project require PHP version >= 7.3
	1. Execute blogdb.sql file to create the database
	2. Edit app/globals.php file with your own mysql connection data
	3. configure virtual host or Start php server with: php -S 127.0.0.1:80 -t public public/index.php
	
Demo accounts (The password are the same as username) :
admin
regular

If you dont know password for some user you can login with his username and md5 hash from password 
You can get it (md5 hash) from DB table 'users', column 'password' :)
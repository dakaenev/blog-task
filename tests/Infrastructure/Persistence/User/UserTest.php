<?php

declare(strict_types=1);

namespace Tests\Domain\User;

use App\Domain\User\User;
use Tests\TestCase;
use App\Infrastructure\Persistence\User\UserRepository;

class UserTest extends TestCase
{
    protected $userRepository;
    public function userProvider()
    {
        return [
            'admin', 'nonadmin'
        ];
    }
    public function testUsernameExists(string $existingUsername, string $nonexistentUsername)
    {
        $this->assertEquals($this->userRepository->isUsernameTaken($existingUsername), true);
        $this->assertEquals($this->userRepository->isUsernameTaken($existingUsername), false);
    }

    public function testUserReaderAction(array $user, array $expected): void
    {
        // Mock the repository result
        $this->mock(UserRepository::class)
            ->method('getUserById')->willReturn($user);

        // Create request with method and url
        $request = $this->createRequest('GET', '/users/1');

        // Make request and fetch response
        $response = $this->app->handle($request);

        // Asserts
        $this->assertSame(200, $response->getStatusCode());
        $this->assertJsonData($expected, $response);
    }

    /**
     * Provider.
     *
     * @return array The data
     */
    public function provideUserReaderAction(): array
    {
        $user = array();
        $user = [
            'id' => 1,
            'username' => 'regular',
            'password' => 'admin',
            'first_name' => 'adminf',
            'last_name' => 'adminl',
            'email' => 'admine@am.mn',
            'created_on' => '2021-10-21',
            'role_id' => 1
        ];

        return [
            'User' => [
                $user,
                [
                    'id' => 1,
                    'username' => 'regular',
                    'password' => 'admin',
                    'first_name' => 'adminf',
                    'last_name' => 'adminl',
                    'email' => 'admine@am.mn',
                    'created_on' => '2021-10-21',
                    'role_id' => 1
                ]
            ]
        ];
    }
}

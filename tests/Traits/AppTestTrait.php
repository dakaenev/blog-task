<?php

namespace App\Test\Traits;

use Selective\TestTrait\Traits\ContainerTestTrait;
use Selective\TestTrait\Traits\MockTestTrait;
use Slim\App;
use UnexpectedValueException;

trait AppTestTrait
{
    use ContainerTestTrait;
    use MockTestTrait;
    // add more traits here if needed

    protected App $app;

    protected function setUp(): void
    {
        $this->app = require __DIR__ . '/../../config/bootstrap.php';

        $container = $this->app->getContainer();
        if ($container === null) {
            throw new UnexpectedValueException('Container must be initialized');
        }

        $this->setUpContainer($container);
    }
}

<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\User;

use App\Domain\User\User;
use App\Domain\Helpers\DatabaseTable;
use App\Domain\Helpers\DatabaseConnector;

class UserRepository extends DatabaseTable
{
    public $_DataBase;
    public $_Primary = 'id';
    public $_TableName = 'Users';
    public $_ColsMetaData = array(
        'ID' => array(
            'COLUMN_NAME' => 'id'
        ),
        'USER' => array(
            'COLUMN_NAME' => 'username'
        ),
        'PASS' => array(
            'COLUMN_NAME' => 'password'
        ),
        'NAME1' => array(
            'COLUMN_NAME' => 'first_name'
        ),
        'NAME2' => array(
            'COLUMN_NAME' => 'last_name'
        ),
        'EMAIL' => array(
            'COLUMN_NAME' => 'email'
        ),
        'CREATED_ON' => array(
            'COLUMN_NAME' => 'created_on'
        ),
        'ROLE_ID' => array(
            'COLUMN_NAME' => 'role_id'
        )
    );


    // Methods
    public function checkUserByName($UserName)
    {
        $CheckUserName = $this->_DataBase->query('SELECT ' . $this->getPrimary() . ' FROM ' . $this->getTable() . ' WHERE ' . $this->USER . ' = "' .  $this->MySQL_RES($UserName) . '"');
        return ((bool) mysqli_num_rows($CheckUserName));
    }

    public function checkUserLogin($UserName, $Password)
    {
        $CheckUserLogin = $this->_DataBase->query('SELECT ' . $this->getPrimary() . ', ' . $this->USER . ', ' . $this->ROLE_ID . ' FROM ' . $this->getTable() . ' WHERE ' . $this->USER . ' = "' .  $this->MySQL_RES($UserName) . '" AND (' . $this->PASS . ' = "' . $this->MySQL_RES($Password) . '" OR ' . $this->PASS . ' = "' . $this->MySQL_RES(md5($Password)) . '") LIMIT 1');
        if (mysqli_num_rows($CheckUserLogin) != 0) {
            $_SESSION['current_user']['id'] = self::mysqli_result($CheckUserLogin, 0, $this->getPrimary());
            $_SESSION['current_user']['user'] = self::mysqli_result($CheckUserLogin, 0, $this->USER);
            $_SESSION['current_user']['role_id'] = self::mysqli_result($CheckUserLogin, 0, $this->ROLE_ID);
        }
        return ((bool) mysqli_num_rows($CheckUserLogin));
    }
		
    public function isUsernameTaken($username)
    {
        $users = $this->get_all();
        foreach ($users as $user) {
            if ($user['username'] === $username)
                return true;
        }
        return false;
    }
    public function getAllUsernames($where = null)
    {
        $users = $this->get_all($where);
        $names[] = array_map(
            function ($user) {
                return $user['username'];
            },
            $users
        );
        return $names;
    }
		
    public function getAllEmails($where = null)
    {
        $users = $this->get_all($where);
        $emails[] = array_map(
            function ($user) {
                return $user['email'];
            },
            $users
        );
        return $emails;
    }
    public function isEmailTaken($email)
    {
        $users = $this->get_all();
        foreach ($users as $user) {
            if ($user['email'] === $email)
                return true;
        }
        return false;
    }

    public function getUserById($id)
    {
        return $this->Row_To_Array($this->getElementById($id));
    }

    public function get_all($where = null)
    {
        return $this->To_Assoc_Array($this->_DataBase->query('SELECT * FROM ' . $this->getTable() . (!is_null($where) ? ' WHERE ' . $where : '') . ' ORDER BY id ASC'));
    }
    public function get_user_role($id)
    {
        return $this->Row_To_Array($this->_DataBase->query('Select role_name from ' . $this->getTable() . ' join roles on ' . $this->ROLE_ID . ' = roles.id where ' . $this->getPrimary() . ' == ' . $this->MySQL_RES($id)));
    }
		
    public function get_all_for_select($where = null)
    {
        $DbTable_Users = new UserRepository($this->_DataBase);
        $rez = array();
        $users = $this->get_all($where);
        foreach ($users as $user) {
            $rez[$user[$DbTable_Users->getPrimary()]] = $user[$DbTable_Users->user];
        }

        return $rez;
    }
}

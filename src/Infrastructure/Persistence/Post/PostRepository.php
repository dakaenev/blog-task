<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Post;

use App\Domain\User\User;
use App\Domain\Helpers\DatabaseTable;
use App\Domain\Helpers\DatabaseConnector;

class PostRepository extends DatabaseTable
{
    public $_DataBase;
    public $_Primary = 'id';
    public $_TableName = 'Posts';
    public $_ColsMetaData = array(
        'ID' => array(
            'COLUMN_NAME' => 'id'
        ),
        'TITLE' => array(
            'COLUMN_NAME' => 'title'
        ),
        'CONTENT' => array(
            'COLUMN_NAME' => 'content'
        ),
        'IMG' => array(
            'COLUMN_NAME' => 'img_path'
        ),
        'CREATED_ON' => array(
            'COLUMN_NAME' => 'created_on'
        ),
        'CREATED_BY' => array(
            'COLUMN_NAME' => 'created_by'
        )
    );


    // Methods
    public function checkPostByTitle($Title)
    {
        $CheckTitle = $this->_DataBase->query('SELECT ' . $this->getPrimary() . ' FROM ' . $this->getTable() . ' WHERE ' . $this->TITLE . ' = "' .  $this->MySQL_RES($Title) . '"');
        return ((bool) mysqli_num_rows($CheckTitle));
    }

    public function getPostsForUser($id = null)
    {

        return $this->To_Assoc_Array($this->_DataBase->query('SELECT ' . $this->getTable() . '.*, username as posted_by from ' . $this->getTable() . ' join users on ' . $this->CREATED_BY . ' = users.id ' . (!is_null($id) ? ' WHERE users.id = ' . $this->MySQL_RES($id) : '')));
    }

    public function getPostById($id)
    {
        return $this->Row_To_Array($this->getElementById($id), MYSQLI_ASSOC);
    }

    public function get_all($where = null)
    {
        return $this->To_Assoc_Array($this->_DataBase->query('SELECT ' . $this->getTable() . '.*, username as posted_by from ' . $this->getTable() . ' join users on ' . $this->CREATED_BY . ' = users.id ' . (!is_null($where) ? ' WHERE ' . $where : '') . ' ORDER BY id ASC'));
    }

    public function get_all_for_select($where = null)
    {
        $DbTable_Posts = new PostRepository($this->_DataBase);
        $rez = array();
        $posts = $this->get_all($where);
        foreach ($posts as $post) {
            $rez[$post[$DbTable_Posts->getPrimary()]] = $post[$DbTable_Posts->post];
        }

        return $rez;
    }
}

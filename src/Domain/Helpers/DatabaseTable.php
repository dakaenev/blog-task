<?php

namespace App\Domain\Helpers;

abstract class DatabaseTable
{

    public $_DataBase;
    public function __construct(DatabaseConnector $db)
    {
        $this->_DataBase = $db;
    }
    public static function join_assoc_arrays(array $array1, array $array2)
    {
        $output = array();

        $arrayJoined = array_merge($array1, $array2);
        foreach ($arrayJoined as $value) {
            $id = $value['id'];
            if (!isset($output[$id])) {
                $output[$id] = array();
            }
            $output[$id] = array_merge($output[$id], $value);
        }

        return $output;
    }

    public static function mysqli_result($res, $row, $field = 0)
    {
        $res->data_seek($row);
        $datarow = $res->fetch_array();
        return $datarow[$field];
    }

    public function MySQL_RES($variable)
    {
        if (is_null($this->_DataBase->_connection)) {
            $this->_DataBase->open();
        }
        return mysqli_real_escape_string($this->_DataBase->_connection, $variable);
    }
    public function get($name)
    {
        $field = (isset($this->_ColsMetaData[strtoupper($name)]) ? $this->_ColsMetaData[strtoupper($name)]['COLUMN_NAME'] : trigger_error('$this->_ColsMetaData[' . $name . '] is undefined!', E_USER_NOTICE));
        return $field;
    }

    public function insert($data)
    {
        $fields = array();
        $values = array();

        foreach ($this->_ColsMetaData as $item) 
				{
            if (isset($data[$item['COLUMN_NAME']])) 
						{
                if ($item['COLUMN_NAME'] != $this->getPrimary()) // Excluding Primary key field
                {
                    $fields[] = '`' . $item['COLUMN_NAME'] . '`';
                    $values[] = '"' . $this->MySQL_RES($data[$item['COLUMN_NAME']]) . '"';
                }
            } 
						else 
						{
            }
        }
        $QText = 'INSERT INTO ' . $this->getTable() . '(' . implode(',', $fields) . ') VALUES(' . implode(',', $values) . ')';
        $QInsert = $this->_DataBase->query($QText);

        if ($QInsert) 
				{
          return mysqli_insert_id($this->_DataBase->_connection);
        } 
				else 
				{
          return false;
        }
    }

    public function update($posted_data, $where)
    {
        $fields = array();

        foreach ($this->_ColsMetaData as $item) 
				{
            if (isset($posted_data[$item['COLUMN_NAME']])) {
                $fields[] = '`' . $item['COLUMN_NAME'] . '` = "' . $this->MySQL_RES($posted_data[$item['COLUMN_NAME']]) . '"';
            }
        }

        $QText = 'UPDATE ' . $this->getTable() . ' SET ' . implode(',', $fields) . ' WHERE ' . $where;
        $QUpdate = $this->_DataBase->query($QText);

        if ($QUpdate) 
				{
          return true;
        } 
				else 
				{
          return false;
        }
    }

    public function update_query($posted_data, $where)
    {
        $fields = array();

        foreach ($posted_data as $k => $item) {
            $fields[] = '`' . $k . '` = "' . $this->MySQL_RES($item) . '"';
        }

        $QText = 'UPDATE ' . $this->getTable() . ' SET ' . implode(',', $fields) . ' WHERE ' . $where;
        $QUpdate = $this->_DataBase->query($QText);

        if ($QUpdate) {
            return true;
        } else {
            return false;
        }
    }

    public function update_by_id($posted_data, $primary_id)
    {
        return $this->update($posted_data, $this->getPrimary() . ' = ' . $primary_id);
    }

    public function delete($where)
    {
        $QText = 'DELETE FROM ' . $this->getTable() . ' WHERE ' . $where;
        //echo $QText; exit;
        return $this->_DataBase->query($QText);
    }

    public function delete_by_id($primary_id)
    {
        return $this->delete($this->getPrimary() . ' = ' . $primary_id);
    }

    public function getPrimary()
    {
        return $this->_Primary;
    }

    public function getTable()
    {
        return $this->_TableName;
    }

    public function Row_To_Array($mysqli_result, $result_type = MYSQLI_BOTH)
    {
        return mysqli_fetch_array($mysqli_result, $result_type);
    }

    public function To_Array($mysqli_result, $result_type = MYSQLI_BOTH)
    {
        $result_array = array();
        if (mysqli_num_rows($mysqli_result) > 0) {
            while ($row = mysqli_fetch_array($mysqli_result, $result_type)) {
                $result_array[] = $row;
            }
        }
        return $result_array;
    }

    public function To_Assoc_Array($mysqli_result)
    {
        return $this->To_Array($mysqli_result, MYSQLI_ASSOC);
    }

    public function To_Numeric_Array($mysqli_result)
    {
        return $this->To_Array($mysqli_result, MYSQLI_NUM);
    }

    public function getElementById($id)
    {
        return $this->_DataBase->query('SELECT * FROM ' . $this->getTable() . ' WHERE ' . $this->getPrimary() . ' = "' .  $this->MySQL_RES($id) . '" LIMIT 1');
    }

    // Redefine Magic Method... This allow to use syntax as $DbTable_Users->ID

    public function __get($name)
    {
        return $this->get($name);
    }
}

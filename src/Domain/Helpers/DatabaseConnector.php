<?php

namespace App\Domain\Helpers;

class DatabaseConnector
{
    public $_connection;
    public $_last_query;

    public function open()
    {
        $this->_connection = mysqli_connect($_ENV['DB_HOST'], $_ENV['DB_USER'], $_ENV['DB_PASS'], $_ENV['DB_NAME'])
            or die('Application can\'t access DataBase Server! <br /> Connect Error (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
        mysqli_query($this->_connection, 'set names utf8');
    }

    public function close()
    {
        mysqli_close($this->_connection);
    }

    public function query($sql)
    {
        if (is_null($this->_connection)) {
            $is_open_yet = false;
            $this->open();
        } else {
            $is_open_yet = true;
        }
        $result = mysqli_query($this->_connection, $sql);
        return $result;
    }

    private function SQL_DEBUG($query)
    {
        if ($query == '') return 0;

        global $SQL_INT;
        if (!isset($SQL_INT)) $SQL_INT = 0;

        $query = preg_replace("/['\"]([^'\"]*)['\"]/i", "'<FONT COLOR='#FF6600'>$1</FONT>'", $query, -1);

        $query = str_ireplace(
            array(
                '*',
                'SELECT ',
                'UPDATE ',
                'DELETE ',
                'INSERT ',
                'INTO',
                'VALUES',
                'FROM',
                'LEFT',
                'JOIN',
                'WHERE',
                'LIMIT',
                'ORDER BY',
                'AND',
                'OR ',
                'DESC',
                'ASC',
                'ON '
            ),
            array(
                "<FONT COLOR='#FF6600'><B>*</B></FONT>",
                "<FONT COLOR='#00AA00'><B>SELECT</B> </FONT>",
                "<FONT COLOR='#00AA00'><B>UPDATE</B> </FONT>",
                "<FONT COLOR='#00AA00'><B>DELETE</B> </FONT>",
                "<FONT COLOR='#00AA00'><B>INSERT</B> </FONT>",
                "<FONT COLOR='#00AA00'><B>INTO</B></FONT>",
                "<FONT COLOR='#00AA00'><B>VALUES</B></FONT>",
                "<FONT COLOR='#00AA00'><B>FROM</B></FONT>",
                "<FONT COLOR='#00CC00'><B>LEFT</B></FONT>",
                "<FONT COLOR='#00CC00'><B>JOIN</B></FONT>",
                "<FONT COLOR='#00AA00'><B>WHERE</B></FONT>",
                "<FONT COLOR='#AA0000'><B>LIMIT</B></FONT>",
                "<FONT COLOR='#00AA00'><B>ORDER BY</B></FONT>",
                "<FONT COLOR='#0000AA'><B>AND</B></FONT>",
                "<FONT COLOR='#0000AA'><B>OR</B> </FONT>",
                "<FONT COLOR='#0000AA'><B>DESC</B></FONT>",
                "<FONT COLOR='#0000AA'><B>ASC</B></FONT>",
                "<FONT COLOR='#00DD00'><B>ON</B> </FONT>"
            ),
            $query
        );

        echo "<FONT COLOR='#0000FF'><B>SQL[" . $SQL_INT . "]:</B> " . $query . "<FONT COLOR='#FF0000'>;</FONT></FONT><BR>\n";
        echo mysqli_error($this->_connection);
        $SQL_INT++;
    }
}

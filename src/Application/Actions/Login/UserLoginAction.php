<?php

declare(strict_types=1);

namespace App\Application\Actions\Login;

use Exception;
use Psr\Http\Message\ResponseInterface as Response;

class UserLoginAction extends LoginAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() == 'POST') {
            $params = $this->request->getParsedBody();
            if ($this->userRepository->checkUserLogin($params['username'], $params['password'])) {
                return $this->response->withStatus(302)->withHeader('Location', '/posts');
            }
        }
        return $this->view->render($this->response, 'login/login.html.twig');
    }
}

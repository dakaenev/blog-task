<?php

declare(strict_types=1);

namespace App\Application\Actions\Login;

use App\Application\Actions\Action;
use App\Infrastructure\Persistence\User\UserRepository;
use Psr\Log\LoggerInterface;

abstract class LoginAction extends Action
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @param LoggerInterface $logger
     * @param UserRepository $userRepository
     */
    public function __construct(
        LoggerInterface $logger,
        UserRepository $userRepository
    ) {
        parent::__construct($logger, $userRepository);
        $this->userRepository = $userRepository;
    }
}

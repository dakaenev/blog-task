<?php

declare(strict_types=1);

namespace App\Application\Actions\Login;

use Exception;
use Psr\Http\Message\ResponseInterface as Response;

class UserLogoutAction extends LoginAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() == 'GET') {
            $params = $this->request->getParsedBody();
            session_unset();
            return $this->response->withStatus(302)->withHeader('Location', '/login');
        }
    }
}

<?php

declare(strict_types=1);

namespace App\Application\Actions\Register;

use Exception;
use Psr\Http\Message\ResponseInterface as Response;

class RegisterUserAction extends RegisterAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        if ($this->request->getMethod() == 'POST') {

            $params = $this->request->getParsedBody();
            if ($this->userRepository->isUsernameTaken($params['username']))
                throw new Exception('User with this username is already taken!');
            if ($this->userRepository->isEmailTaken($params['email']))
                throw new Exception('User with this email is already taken!');


            $this->userRepository->insert([
                'username' => $params['username'],
                'first_name' => $params['first_name'],
                'last_name' => $params['last_name'],
                'password' => $params['password'],
                'email' => $params['email'],
                'created_on' => date('Y-m-d'),
                'role_id' => 2,
            ]);

            return $this->response->withStatus(302)->withHeader('Location', '/login');
        }
        return $this->view->render($this->response, 'login/register.html.twig', [
            'emails' => $this->userRepository->getAllEmails(),
            'usernames' => $this->userRepository->getAllUsernames(),
        ]);
    }
}

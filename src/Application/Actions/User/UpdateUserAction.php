<?php

declare(strict_types=1);

namespace App\Application\Actions\User;

use Psr\Http\Message\ResponseInterface as Response;
use Exception;

class UpdateUserAction extends UserAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        if (!isset($_SESSION['current_user'])) {
            return $this->response->withStatus(302)->withHeader('Location', '/login');
        }
        if ($this->request->getMethod() == 'POST') {
            $params = $this->request->getParsedBody();

            $this->userRepository->update_by_id([
                'username' => $params['username'],
                'first_name' => $params['first_name'],
                'last_name' => $params['last_name'],
                'password' => $params['password'],
                'email' => $params['email'],
                'role_id' => $params['role']
            ], (int) $this->resolveArg('id'));
            return $this->response->withStatus(302)->withHeader('Location', '/users');
        }
        $userId = (int) $this->resolveArg('id');

        $user = $this->userRepository->getUserById($userId);

        $this->logger->info("User of id `${userId}` was viewed.");

        return $this->view->render($this->response, 'users/update.html.twig', [
            'roles' => [
                ['role_id' => 1, 'role_name' => 'admin'],
                ['role_id' => 2, 'role_name' => 'regular']
            ],
            'user' => $user,
            'role_id' => $_SESSION['current_user']['role_id'],
            'user_id' => $_SESSION['current_user']['id'],
            'username' => $_SESSION['current_user']['user'],
            'emails' => $this->userRepository->getAllEmails($this->userRepository->getPrimary() . ' <> ' . $this->userRepository->MySQL_RES($userId)),
            'usernames' => $this->userRepository->getAllUsernames($this->userRepository->getPrimary() . ' <> ' . $this->userRepository->MySQL_RES($userId)),
        ]);
    }
}

<?php

declare(strict_types=1);

namespace App\Application\Actions\User;

use Psr\Http\Message\ResponseInterface as Response;

class DeleteUserAction extends UserAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        if (!isset($_SESSION['current_user'])) {
            return $this->response->withStatus(302)->withHeader('Location', '/login');
        }
        if ($this->request->getMethod() == 'GET') {
            $this->userRepository->delete_by_id((int) $this->resolveArg('id'));
            return $this->response->withStatus(302)->withHeader('Location', '/users');
        }
    }
}

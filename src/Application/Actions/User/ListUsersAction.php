<?php

declare(strict_types=1);

namespace App\Application\Actions\User;

use Psr\Http\Message\ResponseInterface as Response;

class ListUsersAction extends UserAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        if (!isset($_SESSION['current_user'])) {
            return $this->response->withStatus(302)->withHeader('Location', '/login');
        }
        $users = $this->userRepository->get_all();

        $this->logger->info("Users list was viewed.");

        return $this->view->render($this->response, 'users/index.html.twig', [
            'users' => $users,
            'role_id' => $_SESSION['current_user']['role_id'],
            'user_id' => $_SESSION['current_user']['id'],
            'username' => $_SESSION['current_user']['user']
        ]);
    }
}

<?php

declare(strict_types=1);

namespace App\Application\Actions\Post;

use Psr\Http\Message\ResponseInterface as Response;

class DeletePostAction extends PostAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        if (!isset($_SESSION['current_user'])) {
            return $this->response->withStatus(302)->withHeader('Location', '/login');
        }
        if ($this->request->getMethod() == 'GET') {
            $this->postRepository->delete_by_id((int) $this->resolveArg('id'));
            return $this->response->withStatus(302)->withHeader('Location', '/posts');
        }
    }
}

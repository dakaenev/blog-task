<?php

declare(strict_types=1);

namespace App\Application\Actions\Post;

use Psr\Http\Message\ResponseInterface as Response;

class ViewPostAction extends PostAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        if (!isset($_SESSION['current_user'])) {
            return $this->response->withStatus(302)->withHeader('Location', '/login');
        }
        $postId = (int) $this->resolveArg('id');

        $post = $this->postRepository->get_all($this->postRepository->getTable() . '.id = ' . $postId);

        $this->logger->info("Post of id `${postId}` was viewed.");

        return $this->view->render($this->response, 'posts/view.html.twig', [
            'post' => $post,
            'role_id' => $_SESSION['current_user']['role_id'],
            'user_id' => $_SESSION['current_user']['id'],
            'username' => $_SESSION['current_user']['user']
        ]);
    }
}

<?php

declare(strict_types=1);

namespace App\Application\Actions\Post;

use App\Application\Actions\Action;
use App\Infrastructure\Persistence\Post\PostRepository;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

abstract class PostAction extends Action
{
    /**
     * @var PostRepository
     */
    protected $postRepository;

    /**
     * @param LoggerInterface $logger
     * @param PostRepository $userRepository
     */
    public function __construct(
        LoggerInterface $logger,
        PostRepository $postRepository,
        ContainerInterface $container
    ) {
        parent::__construct($logger, $container);
        $this->postRepository = $postRepository;
    }
}

<?php

declare(strict_types=1);

namespace App\Application\Actions\Post;

use Exception;
use Psr\Http\Message\ResponseInterface as Response;

class CreatePostAction extends PostAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        if (!isset($_SESSION['current_user'])) {
            return $this->response->withStatus(302)->withHeader('Location', '/login');
        }
        if ($this->request->getMethod() == 'POST') {
            $params = $this->request->getParsedBody();
            $files = $this->request->getUploadedFiles();
            $myFile = $files['image'];
            $allowed_file_types = ['image/png', 'image/jpeg'];
            $upload_full_path = '';
            if ($myFile->getError() === UPLOAD_ERR_OK) {
                if ($myFile->getSize() > 50000) {
                    throw new Exception('File too big!');
                }
                if (!in_array($myFile->getClientMediaType(), $allowed_file_types)) {
                    throw new Exception('Incorrect File Type');
                }
                $uploadFileName = strtotime('now')  . '_' . $myFile->getClientFilename();
                $upload_path = 'src' . DIRECTORY_SEPARATOR . 'Uploads' . DIRECTORY_SEPARATOR;
                if (file_exists($upload_path)) {
                    $upload_full_path = $upload_path . $uploadFileName;
                    $myFile->moveTo($upload_full_path);
                } else {
                    throw new Exception('Picture Cannot Be Saved');
                }
            }
            $this->postRepository->insert([
                'title' => $params['title'],
                'content' => $params['content'],
                'img_path' => $upload_full_path,
                'created_on' => date('Y-m-d'),
                'created_by' => $_SESSION['current_user']['id']
            ]);
            return $this->response->withStatus(302)->withHeader('Location', '/posts');
        }
        return $this->view->render($this->response, 'posts/create.html.twig', [
            'role_id' => $_SESSION['current_user']['role_id'],
            'user_id' => $_SESSION['current_user']['id'],
            'username' => $_SESSION['current_user']['user']
        ]);
    }
}

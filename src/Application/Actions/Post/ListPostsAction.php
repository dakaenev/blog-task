<?php

declare(strict_types=1);

namespace App\Application\Actions\Post;

use Psr\Http\Message\ResponseInterface as Response;

class ListPostsAction extends PostAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        if (!isset($_SESSION['current_user'])) {
            return $this->response->withStatus(302)->withHeader('Location', '/login');
        }
        $posts = $this->postRepository->getPostsForUser();

        $this->logger->info("Posts list was viewed.");

        return $this->view->render($this->response, 'posts/index.html.twig', [
            'posts' => $posts,
            'role_id' => $_SESSION['current_user']['role_id'],
            'user_id' => $_SESSION['current_user']['id'],
            'username' => $_SESSION['current_user']['user']
        ]);
    }
}

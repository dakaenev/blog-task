<?php

declare(strict_types=1);

namespace App\Application\Actions\Post;

use Psr\Http\Message\ResponseInterface as Response;
use Exception;

class UpdatePostAction extends PostAction
{
    /*
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        if (!isset($_SESSION['current_user'])) {
            return $this->response->withStatus(302)->withHeader('Location', '/login');
        }
        if ($this->request->getMethod() == 'POST') {
            $params = $this->request->getParsedBody();
            $files = $this->request->getUploadedFiles();
            if (empty($files['image'])) {
                echo 'empty';
                die;
            }
            $myFile = $files['image'];
            $allowed_file_types = ['image/png', 'image/jpeg'];
            $myFile = $files['image'];
            $upload_full_path = '';
            if ($myFile->getError() === UPLOAD_ERR_OK) {
                if ($myFile->getSize() > 50000) {
                    echo 'File too Big';
                }
                if (!in_array($myFile->getClientMediaType(), $allowed_file_types)) {
                    echo 'File Extension not Supported';
                }
                $uploadFileName = strtotime('now')  . '_' . $myFile->getClientFilename();
                $upload_path = 'src' . DIRECTORY_SEPARATOR . 'Uploads' . DIRECTORY_SEPARATOR;
                if (file_exists($upload_path)) {
                    $current_img_path = $this->postRepository->getPostById($this->resolveArg('id'))['img_path'];
                    if (file_exists($current_img_path)) {
                        unlink($current_img_path);
                    }
                    $upload_full_path = $upload_path . $uploadFileName;
                    $myFile->moveTo($upload_full_path);
                } else {
                    throw new Exception('Picture Cannot Be Saved');
                }
            }
            $this->postRepository->update_by_id([
                'title' => $params['title'],
                'content' => $params['content'],
                'img_path' => $upload_full_path,
                'created_on' => date('Y-m-d'),
                'created_by' => $_SESSION['current_user']['id'],
            ], (int) $this->resolveArg('id'));
            return $this->response->withStatus(302)->withHeader('Location', '/posts');
        }
        $postId = (int) $this->resolveArg('id');

        $post = $this->postRepository->getPostById($postId);

        $this->logger->info("Post of id `${postId}` was viewed.");

        return $this->view->render($this->response, 'posts/update.html.twig', [
            'post' => $post, 'role_id' => $_SESSION['current_user']['role_id'],
            'user_id' => $_SESSION['current_user']['id'],
            'username' => $_SESSION['current_user']['user']
        ]);
    }
}
